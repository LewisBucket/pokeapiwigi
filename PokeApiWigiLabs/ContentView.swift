//
//  ContentView.swift
//  PokeApiWigiLabs
//
//  Created by Pedro Ferreira on 26/10/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            PokeBackground()
            PokemonList().frame(width: 300, height: 400, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

