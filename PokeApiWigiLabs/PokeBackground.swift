//
//  PokeBackground.swift
//  PokeApiWigiLabs
//
//  Created by Pedro Ferreira on 26/10/21.
//

import SwiftUI

struct PokeBackground: View {
    var body: some View {
        VStack(spacing: 0){
            Color.blue
            //    .frame(width: .greatestFiniteMagnitude, height: 500)
            Color.black
        }
    }
}

struct PokeBackground_Previews: PreviewProvider {
    static var previews: some View {
        PokeBackground()
    }
}
