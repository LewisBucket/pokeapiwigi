//
//  PokemonModel.swift
//  PokeApiWigiLabs
//
//  Created by Pedro Ferreira on 26/10/21.
//

import Foundation

struct PokemonResponse: Codable {
    let count: Int
    let results: [Pokemon]
}

struct Pokemon: Identifiable, Hashable, Codable {
    var id: Int?
    let name: String
    let image: Data?
    var isLike: Bool?
    
    mutating func setLike(isLike: Bool) {
        self.isLike = isLike
    }
}

