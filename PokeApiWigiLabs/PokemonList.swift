//
//  Othermodel.swift
//  PokeApiWigiLabs
//
//  Created by Pedro Ferreira on 26/10/21.
//


import SwiftUI

struct PokemonList: View {
    
    @State private var searchText = ""
    @ObservedObject var viewModel = PokemonViewModel()
 //   @State var showAler = false
    
    var body: some View {
        
            NavigationView {
                VStack {
                    Spacer(minLength: 10)
//                    Image(uiImage: UIImage(named: "pokemon-go-evolucionar")!)
//                        .resizable()
//                        .clipped()
//                        .frame(height: 200
//                               , alignment: .center)
//                        .background(Color.red)
//                    HStack {
//                        NavigationLink(destination: TinderView(listPokemon: self.viewModel.items)){
//                            Detail(text: "Vote Pokemon")
//                        }
//                    }
                    
                    VStack(spacing: 10){
                        SearchBar(text: self.$searchText).accentColor(.primary)
                        List(self.viewModel.filter(by: self.searchText)){ pokemon in
                            Text(pokemon.name).font(.headline)
//                            NavigationLink(destination: PokemonDetailView(pokemon: pokemon)) {
//                                PokemonRow(pokemon: pokemon)
//                            }
                        }
                        .listRowBackground(Color(red: 0, green: 191, blue: 255))
                        .onAppear {
                            if self.viewModel.items.isEmpty {
                                self.viewModel.getPokemon()
                            } else {
                              //  self.viewModel.isShowingLoading = false
                            }
                        }
//                        .alert(isPresented: self.$showAler){ () -> Alert in
//                            Alert(title: Text("Error"),
//                                  message: Text (self.viewModel.errorMessage))
//                        }
                    }.background(Color.white)
                }
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarHidden(true)
            }
    }
}

struct PokemonList_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PokemonList()
            PokemonList()
                .previewDevice("iPhone SE (2nd generation)")
            PokemonList()
                .previewDevice("iPhone 8")
        }
    }
}

